package com.developer.dataid.activity;

import com.developer.dataid.model.DatosResuesta;

/**
 * Created by dessis-aux42 on 28/08/17.
 */

public interface IDataIDListener {

  void consultaExitosa(DatosResuesta datosResuesta);
  void consultaFallida(DatosResuesta datosResuesta);
}
