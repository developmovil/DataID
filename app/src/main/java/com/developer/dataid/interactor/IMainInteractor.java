package com.developer.dataid.interactor;

import com.developer.dataid.presenter.IMainPresenter;

/**
 * Created by dessis-aux42 on 23/08/17.
 */

public interface IMainInteractor {

  void consultarOCR(IMainPresenter.OnConsultaOCRFinishedListener ocrListener, String base64, String api_key);

}
