package com.developer.dataid.permission;

public interface PermisoListener {
  void NuncaPreguntado();

  void Permitido();

  void Denegado();

  void Bloqueado();
}
