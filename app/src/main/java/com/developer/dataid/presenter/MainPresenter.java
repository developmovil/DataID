package com.developer.dataid.presenter;

import android.content.Context;

import com.developer.dataid.activity.IDataIDListener;
import com.developer.dataid.activity.MainView;
import com.developer.dataid.interactor.MainInteractor;
import com.developer.dataid.model.CredencialElector;
import com.developer.dataid.model.DatosResuesta;
import com.developer.dataid.model.ErrorOCR;

/**
 * Created by dessis-aux42 on 23/08/17.
 */

public class MainPresenter implements IMainPresenter,
IMainPresenter.OnConsultaOCRFinishedListener{

  private MainInteractor mainInteractor;
  private IDataIDListener iDataIDListener;

  public MainPresenter(IDataIDListener iDataIDListener){
    this.mainInteractor = new MainInteractor();
    this.iDataIDListener = iDataIDListener;
  }

  @Override
  public void llamarOCR(String base64, String api_key) {
    mainInteractor.consultarOCR(this, base64, api_key);
  }

  @Override
  public void onSuccess(String respuesta) {
    CredencialElector credencialElector = new CredencialElector("Pedro","Torres","Perez"
        ,"","","","","","","","");
    DatosResuesta datosResuesta = new DatosResuesta(null, credencialElector);
    iDataIDListener.consultaExitosa(datosResuesta);
  }

  @Override
  public void onFailed(String error) {
    ErrorOCR errorOCR = new ErrorOCR(error);
    DatosResuesta datosResuesta = new DatosResuesta(errorOCR, null);
    iDataIDListener.consultaFallida(datosResuesta);
  }
}
