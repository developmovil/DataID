package com.developer.dataid.service;

import com.developer.dataid.model.RespuestaOCR;
import com.developer.dataid.request.SolicitudOCR;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by dessis-aux42 on 23/08/17.
 */

public interface IMainServicios {
  @POST("v1/images:annotate")
  Call<RespuestaOCR> getDataOCR(@Query("key") String key, @Body SolicitudOCR solicitudOCR);
}
