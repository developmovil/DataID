package com.developer.extracttextocr.interactor;


import com.developer.extracttextocr.presenter.IMainPresenter;

/**
 * Created by dessis-aux42 on 23/08/17.
 */

public interface IMainInteractor {

  void consultarOCR(IMainPresenter.OnConsultaOCRFinishedListener ocrListener, String base64, String api_key);

}
