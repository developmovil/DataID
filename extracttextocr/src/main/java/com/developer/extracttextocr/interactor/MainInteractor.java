package com.developer.extracttextocr.interactor;

import android.util.Log;

import com.developer.extracttextocr.model.RespuestaOCR;
import com.developer.extracttextocr.presenter.IMainPresenter;
import com.developer.extracttextocr.request.SolicitudOCR;
import com.developer.extracttextocr.service.IMainServicios;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static android.content.ContentValues.TAG;

/**
 * Created by dessis-aux42 on 23/08/17.
 */

public class MainInteractor implements IMainInteractor {

  private String URL = "https://vision.googleapis.com/";
  private String TYPE = "TEXT_DETECTION";

  public MainInteractor() { }

  @Override
  public void consultarOCR(final IMainPresenter.OnConsultaOCRFinishedListener ocrListener, String base64, String api_key) {
    try {

      IMainServicios iMainServicios = configurarRetro();
      SolicitudOCR solicitudOCR = new SolicitudOCR();
      SolicitudOCR.Caracteristicas caracteristicas = new SolicitudOCR.Caracteristicas(1, TYPE);
      SolicitudOCR.ImagenBase64 imagenBase64 = new SolicitudOCR.ImagenBase64(base64);
      SolicitudOCR.Solicitud solicitud = new SolicitudOCR.Solicitud(imagenBase64, Arrays.asList(caracteristicas));
      solicitudOCR.setRequests(Arrays.asList(solicitud));

      Call<RespuestaOCR> call = iMainServicios.getDataOCR(api_key, solicitudOCR);
      call.clone()
          .enqueue(
              new Callback<RespuestaOCR>() {
                @Override
                public void onResponse(Call<RespuestaOCR> call, Response<RespuestaOCR> response) {
                  if (response.isSuccessful() && response.body() != null) {
                    RespuestaOCR respuestaOCR = response.body();
                    if(respuestaOCR.getResponses() != null &&
                        respuestaOCR.getResponses().get(0).getTextAnnotations() != null &&
                        respuestaOCR.getResponses().get(0)
                            .getTextAnnotations().get(0).getDescription() != null ){
                      String descripcion = respuestaOCR
                          .getResponses().get(0)
                          .getTextAnnotations().get(0)
                          .getDescription();
                      ocrListener.onSuccess(descripcion);
                    }else {
                      ocrListener.onFailed("No se pudo obtener informacion");
                    }
                  } else {
                    ocrListener.onFailed("No se pudo obtener informacion");
                  }
                }

                @Override
                public void onFailure(Call<RespuestaOCR> call, Throwable t) {
                  ocrListener.onFailed("No se pudo obtener informacion");
                }
              });
    } catch (Exception error) {
      Log.e(TAG + ":consultarOCR", error.toString());
      ocrListener.onFailed(error.getMessage());
    }
  }

  public IMainServicios configurarRetro() throws Exception {
    Retrofit retrofit;
    try {
      OkHttpClient.Builder httpClient =
          new OkHttpClient.Builder()
              .connectTimeout(45, TimeUnit.SECONDS)
              .readTimeout(45, TimeUnit.SECONDS)
              .writeTimeout(45, TimeUnit.SECONDS);
      Gson gson = new GsonBuilder().setLenient().create();
      retrofit =
          new Retrofit.Builder()
              .baseUrl(URL)
              .addConverterFactory(GsonConverterFactory.create(gson))
              .client(httpClient.build())
              .build();
      return retrofit.create(IMainServicios.class);
    } catch (Exception e) {
      Log.e(TAG + ":createService", e.toString());
      return null;
    }

  }
}
