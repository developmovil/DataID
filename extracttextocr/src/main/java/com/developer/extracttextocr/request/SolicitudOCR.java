package com.developer.extracttextocr.request;

import java.util.List;

/**
 * Created by dessis-aux42 on 23/08/17.
 */

public class SolicitudOCR {
  public List<Solicitud> requests;

  public List<Solicitud> getRequests() {
    return requests;
  }
  public void setRequests(List<Solicitud> requests) {
    this.requests = requests;
  }


  public static class Solicitud{
    public Solicitud(ImagenBase64 image, List<Caracteristicas> features) {
      this.image = image;
      this.features = features;
    }

    public ImagenBase64 image;
    public List<Caracteristicas> features;

    public ImagenBase64 getImage() {
      return image;
    }
    public List<Caracteristicas> getFeatures() {
      return features;
    }
  }

  public static class ImagenBase64{

    public ImagenBase64(String content) {
      this.content = content;
    }

    public String content;

    public String getContent() {
      return content;
    }
  }

  public static class Caracteristicas{
    public Caracteristicas(int maxResults, String type) {
      this.maxResults = maxResults;
      this.type = type;
    }

    public int maxResults;
    public String type;

    public int getMaxResults() {
      return maxResults;
    }
    public String getType() {
      return type;
    }
  }
}
