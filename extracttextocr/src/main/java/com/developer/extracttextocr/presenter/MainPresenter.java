package com.developer.extracttextocr.presenter;


import com.developer.extracttextocr.builder.IDataIDListener;
import com.developer.extracttextocr.interactor.MainInteractor;
import com.developer.extracttextocr.model.CredencialElector;
import com.developer.extracttextocr.model.DatosResuesta;
import com.developer.extracttextocr.model.ErrorOCR;

/**
 * Created by dessis-aux42 on 23/08/17.
 */

public class MainPresenter implements IMainPresenter,
IMainPresenter.OnConsultaOCRFinishedListener{

  private MainInteractor mainInteractor;
  private IDataIDListener iDataIDListener;

  public MainPresenter(IDataIDListener iDataIDListener){
    this.mainInteractor = new MainInteractor();
    this.iDataIDListener = iDataIDListener;
  }

  @Override
  public void llamarOCR(String base64, String api_key) {
    mainInteractor.consultarOCR(this, base64, api_key);
  }

  @Override
  public void onSuccess(String respuesta) {
    CredencialElector credencialElector = new CredencialElector("Pedro","Torres","Perez"
        ,"","","","","","","","");
    DatosResuesta datosResuesta = new DatosResuesta(null, credencialElector);
    iDataIDListener.consultaExitosa(datosResuesta);
  }

  @Override
  public void onFailed(String error) {
    ErrorOCR errorOCR = new ErrorOCR(error);
    DatosResuesta datosResuesta = new DatosResuesta(errorOCR, null);
    iDataIDListener.consultaFallida(datosResuesta);
  }
}
