package com.developer.extracttextocr.presenter;

/**
 * Created by dessis-aux42 on 23/08/17.
 */

public interface IMainPresenter {

  void llamarOCR(String base64, String api_key);

  interface OnConsultaOCRFinishedListener{
    void onSuccess(String respuesta);
    void onFailed(String error);
  }

}
