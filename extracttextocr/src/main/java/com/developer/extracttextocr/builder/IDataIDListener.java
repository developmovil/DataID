package com.developer.extracttextocr.builder;

import com.developer.extracttextocr.model.DatosResuesta;

/**
 * Created by dessis-aux42 on 28/08/17.
 */

public interface IDataIDListener {

  void consultaExitosa(DatosResuesta datosResuesta);
  void consultaFallida(DatosResuesta datosResuesta);
}
