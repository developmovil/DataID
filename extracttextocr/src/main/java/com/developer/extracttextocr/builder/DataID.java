package com.developer.extracttextocr.builder;

import com.developer.extracttextocr.presenter.MainPresenter;

/**
 * Creado por Pedro Torres el 28/08/17.
 */

public class DataID {

  /**
   * Variables privadas
   */
  private MainPresenter MAIN_PRESENTER;
  private String API_KEY;
  private String BASE64;

  /**
   * @param builder copia todos sus valores.
   */
  public DataID(DataIDBuilder builder) {
    this.API_KEY = builder.API_KEY;
    this.BASE64 = builder.BASE64;
    this.MAIN_PRESENTER = builder.MAIN_PRESENTER;
  }

  public String getApiKey(){
    return API_KEY;
  }
  public String getBase64(){
    return BASE64;
  }

  public void consultarOCR(){
    MAIN_PRESENTER.llamarOCR(BASE64, API_KEY);
  }

  public static class DataIDBuilder{
    private MainPresenter MAIN_PRESENTER;
    private String API_KEY;
    private String BASE64;

    public DataIDBuilder (IDataIDListener iDataIDListener){
      MAIN_PRESENTER = new MainPresenter(iDataIDListener);
    }
    public DataIDBuilder setApiKey(String apikey){
      this.API_KEY = apikey;
      return this;
    }
    public DataIDBuilder setBase64(String base64){
      this.BASE64 = base64;
      return this;
    }
    public DataID build() {
      return  new DataID(this);
    }
  }
}
