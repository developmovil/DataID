package com.developer.extracttextocr.model;

import android.support.annotation.NonNull;

/**
 * Created by dessis-aux42 on 28/08/17.
 */

public class CredencialElector {
  public CredencialElector() {
  }

  public CredencialElector(@NonNull String nombre, @NonNull String apellidoPaterno, @NonNull String apellidoMaterno,
                           @NonNull String domicilio, @NonNull String colonia, @NonNull String numeroCasa, @NonNull String curp,
                           @NonNull String folio, @NonNull String sexo, @NonNull String edad, @NonNull String tipoCredencial) {
    this.nombre = nombre;
    this.apellidoPaterno = apellidoPaterno;
    this.apellidoMaterno = apellidoMaterno;
    this.domicilio = domicilio;
    this.colonia = colonia;
    this.numeroCasa = numeroCasa;
    this.curp = curp;
    this.folio = folio;
    this.sexo = sexo;
    this.edad = edad;
    this.tipoCredencial = tipoCredencial;
  }

  public String nombre;
  private String apellidoPaterno;
  private String apellidoMaterno;
  private String domicilio;
  private String colonia;
  private String numeroCasa;
  private String curp;
  private String folio;
  private String sexo;
  private String edad;
  private String tipoCredencial;

  public String getNombre() {
    return nombre;
  }

  public void setNombre(@NonNull String nombre) {
    this.nombre = nombre;
  }

  public String getApellidoPaterno() {
    return apellidoPaterno;
  }

  public void setApellidoPaterno(@NonNull String apellidoPaterno) {
    this.apellidoPaterno = apellidoPaterno;
  }

  public String getApellidoMaterno() {
    return apellidoMaterno;
  }

  public void setApellidoMaterno(@NonNull String apellidoMaterno) {
    this.apellidoMaterno = apellidoMaterno;
  }

  public String getDomicilio() {
    return domicilio;
  }

  public void setDomicilio(@NonNull String domicilio) {
    this.domicilio = domicilio;
  }

  public String getColonia() {
    return colonia;
  }

  public void setColonia(@NonNull String colonia) {
    this.colonia = colonia;
  }

  public String getNumeroCasa() {
    return numeroCasa;
  }

  public void setNumeroCasa(@NonNull String numeroCasa) {
    this.numeroCasa = numeroCasa;
  }

  public String getCurp() {
    return curp;
  }

  public void setCurp(@NonNull String curp) {
    this.curp = curp;
  }

  public String getFolio() {
    return folio;
  }

  public void setFolio(@NonNull String folio) {
    this.folio = folio;
  }

  public String getSexo() {
    return sexo;
  }

  public void setSexo(@NonNull String sexo) {
    this.sexo = sexo;
  }

  public String getEdad() {
    return edad;
  }

  public void setEdad(@NonNull String edad) {
    this.edad = edad;
  }

  public String getTipoCredencial() {
    return tipoCredencial;
  }

  public void setTipoCredencial(@NonNull String tipoCredencial) {
    this.tipoCredencial = tipoCredencial;
  }
}
