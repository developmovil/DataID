package com.developer.extracttextocr.model;

/**
 * Created by dessis-aux42 on 28/08/17.
 */

public class DatosResuesta {

  public DatosResuesta(ErrorOCR errorOCR, CredencialElector credencialElector) {
    this.errorOCR = errorOCR;
    this.credencialElector = credencialElector;
  }

  private ErrorOCR errorOCR;
  private CredencialElector credencialElector;

  public ErrorOCR getErrorOCR() {
    return errorOCR;
  }

  public CredencialElector getCredencialElector() {
    return credencialElector;
  }
}
