package com.developer.extracttextocr.model;

import java.util.List;

/**
 * Created by dessis-aux42 on 23/08/17.
 */

public class RespuestaOCR {

  public List<Anotaciones> responses;
  public List<Anotaciones> getResponses() {
    return responses;
  }

  public static class Anotaciones{
    public List<Descripcion> textAnnotations;
    public List<Descripcion> getTextAnnotations() {
      return textAnnotations;
    }
  }

  public static class Descripcion{
    public String description;
    public String getDescription() {
      return description;
    }
  }

}
