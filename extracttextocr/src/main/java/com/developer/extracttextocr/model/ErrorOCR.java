package com.developer.extracttextocr.model;

import android.support.annotation.NonNull;

/**
 * Created by dessis-aux42 on 28/08/17.
 */

public class ErrorOCR {
  public ErrorOCR(@NonNull String mensaje) {
    this.mensaje = mensaje;
  }

  private String mensaje;

  public String getMensaje() {
    return mensaje;
  }
}
