package com.developer.extracttextocr.permission;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.Arrays;
import java.util.List;

public class PreferenciaPermiso {
  private static final String SHARED_PREFS_FILE = "HMPrefs";
  private static final String NEVER_ASKED_CHECKED = "neverasquedchecked";
  private static final String PERMISOS = "permisos";
  private Context mContext;

  public PreferenciaPermiso(Context context) {
    mContext = context;
  }

  private SharedPreferences getSettings() {
    return mContext.getSharedPreferences(SHARED_PREFS_FILE, 0);
  }

  public void setNeverAskedChecked(boolean prChecked) {
    SharedPreferences.Editor editor = getSettings().edit();
    editor.putBoolean(NEVER_ASKED_CHECKED, prChecked);
    editor.apply();
  }

  public List<String> getListaPermisos() {
    return Arrays.asList(getPermisos().split("[&]"));
  }

  public String getPermisos() {
    return getSettings().getString(PERMISOS, "");
  }

  public void setPermiso(String prPermiso) {
    String permisos = getPermisos();
    SharedPreferences.Editor editor = getSettings().edit();
    editor.putString(
        PERMISOS, !getListaPermisos().contains(prPermiso) ? permisos + prPermiso + "&" : permisos);
    editor.apply();
  }
}
