package com.developer.extracttextocr.permission;

import android.app.Activity;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

import java.util.List;

public class ValidarPermiso {

  private boolean primeraPeticion;
  private PermisoListener permisoListener;
  private PreferenciaPermiso preferenciaPermiso;
  private Activity mActivity;
  private String mPermiso;

  public ValidarPermiso(Activity activity, String permiso) {
    this.mActivity = activity;
    this.mPermiso = permiso;
    this.primeraPeticion = true;
    this.preferenciaPermiso = new PreferenciaPermiso(mActivity);
  }

  public void checar() {
    if (ContextCompat.checkSelfPermission(mActivity, mPermiso)
        != PackageManager.PERMISSION_GRANTED) {
      if (!ActivityCompat.shouldShowRequestPermissionRationale(mActivity, mPermiso)) {
        if (!primeraPeticionDelPermiso(mPermiso)) {
          preferenciaPermiso.setPermiso(mPermiso);
          preferenciaPermiso.setNeverAskedChecked(true);
          permisoListener.Bloqueado();
        } else {
          preferenciaPermiso.setNeverAskedChecked(false);
          permisoListener.NuncaPreguntado();
        }
      } else {
        preferenciaPermiso.setPermiso(mPermiso);
        permisoListener.Denegado();
      }
    } else {
      preferenciaPermiso.setPermiso(mPermiso);
      permisoListener.Permitido();
    }
  }

  public void setOnPermisoListener(PermisoListener oPermisoListener) {
    this.permisoListener = oPermisoListener;
  }

  public boolean primeraPeticionDelPermiso(String permiso) {
    List<String> permisos = preferenciaPermiso.getListaPermisos();
    boolean permisoRegistrado = permisos.contains(permiso);
    return !permisoRegistrado;
  }
}
