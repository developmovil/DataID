package com.developer.extracttextocr.permission;

public interface PermisoListener {
  void NuncaPreguntado();

  void Permitido();

  void Denegado();

  void Bloqueado();
}
